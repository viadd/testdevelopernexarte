import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ServiceManagementService } from './service-management.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private service: ServiceManagementService
  ) {
    
  }

  canActivate(): boolean{
    if (this.service.loggedIn()) {
      return true;
    }
    return false
  }
  
}
