import { Component,Inject, OnInit } from '@angular/core';
import { login } from "../sign-up/register.model";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MainPageComponent } from '../main-page/main-page.component';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<MainPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: login
    
  ) { }

  ngOnInit(): void {
  }

  signIn(){
    this.data = {
      email: this.data.email,
      password: this.data.password
    }
    console.log(this.data)

  }


}
