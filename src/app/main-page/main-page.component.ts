import { Component, OnInit } from '@angular/core';
import { ServiceManagementService } from '../service-management.service'
import { MatDialog } from '@angular/material/dialog';
import { SignInComponent } from '../sign-in/sign-in.component'
import { SignUpComponent } from '../sign-up/sign-up.component';
import { mail } from '../sign-up/register.model';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  data : any;
  loggedIn: any;
  mail : mail[] = []

  constructor(
    private service : ServiceManagementService,
    public dialog: MatDialog,
    private snackBar : MatSnackBar

  ) { }

  ngOnInit(): void {
    this.fetchData();
    this.loggedIn = this.service.loggedIn()
    console.log(this.loggedIn)
  }


  fetchData(){
    this.service.getAllData().subscribe(data => {
      this.data = data
    })

  }

  openSignIn() {
    const dialogRef = this.dialog.open(SignInComponent, {
      data: {}
    });
    dialogRef.afterClosed().subscribe(data => {
      this.service.signIn(data).subscribe( res => {
        console.log(res)
        localStorage.setItem('token', res.token)
        localStorage.setItem('name', res.name);
        localStorage.setItem('email', res.email);
        this.loggedIn = this.service.loggedIn()
        this.snackBar.open("Se Ha Iniciado Sesion Correctamente","OK")
      }, err =>{
        console.log(err.error.message)
        this.snackBar.open(err.error.message, "OK")

      })
      console.log(data)
    })
  }

  openSignUp(){
    const dialogRef = this.dialog.open(SignUpComponent, {
      data: {}
    })
    dialogRef.afterClosed().subscribe(data => {
      this.service.createUser(data).subscribe(res => {
        console.log(res)
        localStorage.setItem('token', res.body.token);
        localStorage.setItem('name', res.body.name);
        localStorage.setItem('email', res.body.email);
        this.loggedIn = this.service.loggedIn()
      }, error =>{
        console.log(error)
      });
      console.log(data)
    })
  }

  logOut(){
    localStorage.removeItem('token');
    localStorage.removeItem('name');
    localStorage.removeItem('email');
    this.loggedIn = this.service.loggedIn();
    this.snackBar.open("Sesión Cerrada","OK")
  }


  sendEmail(sendEmailPermit: any){
    console.log(sendEmailPermit)
    let data : any = this.getDataLocalStorage(sendEmailPermit)
    this.service.sendEmail(data).subscribe( res =>{
      this.mail = [];
      console.log(res.correo)
      if (res.correo) { this.snackBar.open("La cotización se ha enviado a su Correo", "OK") } 
    })
  }

  getDataLocalStorage(data: any){
    let name: any = localStorage.getItem('name');
    let email: any = localStorage.getItem('email');
    let token: any = localStorage.getItem('token');
    this.mail.push({
      name:  name,
      email: email,
      token: token,
      carName : data.name,
      carPrice : data.price,
      carInfo : data.info
    })
    return this.mail
  }


}
