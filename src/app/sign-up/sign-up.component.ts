import { Component, Inject, OnInit} from '@angular/core';
import { personas } from './register.model'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MainPageComponent } from '../main-page/main-page.component';
import {FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  nameField = new FormControl('', [Validators.required]);
  passwordField = new FormControl('', [Validators.required])
  phoneField = new FormControl('', [Validators.required]);
  emailField = new FormControl('', [Validators.required, Validators.email]);
  departmentField = new FormControl('', [Validators.required]);
  cityField = new FormControl('', [Validators.required]);
  checkBotton = new FormControl('', [Validators.required])


  constructor(
    public dialogRef: MatDialogRef<MainPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: personas,
    private snackBar : MatSnackBar
  ) { }

  ngOnInit(): void {
    this.onlyLetters();
    this.limitNumb();
  }

  createUser(){
    this.data = {
      name: this.data.name,
      phone: this.data.phone,
      email: this.data.email,
      department: this.data.department,
      city: this.data.city,
      password: this.data.password,
    }
    console.log(this.data)
  }

  openSnackBar() {

    this.snackBar.open('Registrado Correctamente', 'cerrar');

  }

  getErrorMessage() {
    if (this.emailField.hasError('required')) {
      return 'Debe Ingresar su correo por favor';
    }

    if (this.emailField.hasError('email')) {
      return "Correo Invalido"
    }

    return 

  }

  getErrorMessagePhone() {

    if (this.phoneField.hasError('required')) {
      return 'Debe Ingresar un numero celular por favor';
    }

    if (this.phoneField.hasError('minLength', 'maxLength')) {
      return "Numero Invalido"
    }

    return 

  }

  limitNumb() {
    this.phoneField.valueChanges.subscribe( (value) =>{
      let newValue = String(value)
      if (newValue.length > 10) {
        this.snackBar.open("Estas seguro de que ese telefono existe", "OK")
      }
    })
  }

  getErrorMessageName() {
    if (this.nameField.hasError('required')) {
      return 'Debe Ingresar su nombre por favor';
    } 

    return
     
  }

  onlyLetters(){
    this.nameField.valueChanges.subscribe( value =>{
      for (const item in value) {
        if (value[item] == "0" || value[item] === "1" || value[item] === "2" || value[item] === "3" || value[item] === "4" || value[item] === "5" || value[item] === "6"
            || value[item] === "7" || value[item] === "8" || value[item] === "9") {
          this.nameField.setValue("");
          this.snackBar.open("No se permiten numeros en el nombre","OK")
        }
      }
    })
  }

  getErrorMessagePassword() {

    if (this.passwordField.hasError('required')) {
      return 'Debe Ingresar su contraseña por favor';
    }

    return 

  }

  getErrorMessageDepartment() {

    if (this.departmentField.hasError('required')) {
      return 'Seleccione el departamento por favor';
    }

    return 

  }

  getErrorMessageCity() {

    if (this.cityField.hasError('required')) {
      return 'Seleccione la ciudad por favor';
    }

    return 

  }

  getErrorMessageCheckBotton() {

    if (this.checkBotton.hasError('required')) {
      return 'Acepta para continuar';
    }

    return 

  }

}
