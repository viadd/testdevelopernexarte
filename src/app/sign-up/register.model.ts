export interface personas {
    name: string,
    phone: number,
    email: string,
    department: string,
    city: string,
    password: string
}

export interface login {
    email: string
    password: string
}

export interface mail {
    name: string,
    email: string,
    token: string,
    carName: string,
    carPrice: string,
    carInfo: string
}