import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { CatalogComponent } from './catalog/catalog.component'
import { MainPageComponent } from './main-page/main-page.component'

import { AuthGuard } from "./auth.guard";

const routes: Routes = [
  {
    path: 'home',
    component: MainPageComponent
    //canActivate: [AuthGuard]
  },
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
