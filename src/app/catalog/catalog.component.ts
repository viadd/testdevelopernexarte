import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit, OnChanges {

  @Input() getData: any;
  @Input() loggedIn: any;
  @Output() sendEmailPermit: EventEmitter<any> = new EventEmitter();
  data: any;


  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  
  ngOnChanges() : void {
    console.log(this.getData)
  }

  sendEmail(nameCar: any, priceCar: any, infoCar:any) {
    this.sendEmailPermit.emit({
      name: nameCar,
      price: priceCar,
      info: infoCar
    })
  }

}
