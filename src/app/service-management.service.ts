import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { personas, login } from './sign-up/register.model';



@Injectable({
  providedIn: 'root'
})
export class ServiceManagementService {

  headerConf = new HttpHeaders().set("Access-Control-Allow-Origin" ,'request-originating server addresses');

  constructor(
    private http: HttpClient,

  ) { 

  }

  getAllData(){
    return this.http.get('https://integrador.processoft.com.co/api/menutest');
  }

  createUser(persona: personas){
    return this.http.post<any>('http://localhost:3000/users', persona, {headers : this.headerConf});
  }

  signIn(login : login){
    return this.http.post<any>('http://localhost:3000/signIn', login)
  }

  loggedIn(){
    return !!localStorage.getItem('token')
  }

  sendEmail(email: any){
    return this.http.post<any>('http://localhost:3000/sendEmail', email);
  }

}
